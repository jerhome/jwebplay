-- phpMyAdmin SQL Dump
-- version 4.7.0-dev
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2017 at 08:26 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `jwebplay`
--

-- --------------------------------------------------------

--
-- Table structure for table `mediafile`
--

CREATE TABLE `mediafile` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `size` int(11) NOT NULL,
  `md5` varchar(64) NOT NULL,
  `mime` varchar(32) NOT NULL,
  `info` text NOT NULL,
  `format_name` varchar(255) DEFAULT NULL,
  `format_long_name` varchar(255) DEFAULT NULL,
  `duration` float DEFAULT NULL,
  `bit_rate` int(11) DEFAULT NULL,
  `tags` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mediafile`
--
ALTER TABLE `mediafile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mediafile`
--
ALTER TABLE `mediafile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
