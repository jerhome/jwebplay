<?php
namespace Aura\Web_Project\_Config;

use Aura\Di\Config;
use Aura\Di\Container;

class Prod extends Config
{
    public function define(Container $di)
    {
        $di->params['Aura\Sql\ExtendedPdo'] = array(
            'dsn' => 'mysql:host=localhost;dbname=jwebplay',
            'username' => 'jwebplay',
            'password' => 'bNh6BDEBSAuAQjUL'
        );
    
        $di->set(
            'ffprobe',
            \FFMpeg\FFProbe::create([
                    'ffmpeg.threads'   => 4,
                    'ffmpeg.timeout'   => 1,
                    'ffmpeg.binaries'  => '/usr/local/bin/ffmpeg',
                    'ffprobe.timeout'  => 1,
                    'ffprobe.binaries' => '/usr/local/bin/ffprobe',
                ]
            )
        );

    }

    public function modify(Container $di)
    {

    }
}
