<?php
namespace Aura\Web_Project\_Config;

use Aura\Di\Config;
use Aura\Di\Container;

class Dev extends Config
{
    public function define(Container $di)
    {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', true);

        $di->params['Aura\Sql\ExtendedPdo'] = [
            'dsn' => 'mysql:host=localhost;dbname=jwebplay',
            'username' => 'jwebplay',
            'password' => 'bNh6BDEBSAuAQjUL'
        ];

        try {
            $di->set(
                'ffprobe',
                \FFMpeg\FFProbe::create([
                    'ffmpeg.threads'   => 4,
                    'ffmpeg.timeout'   => 1,
                    'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
                    'ffprobe.timeout'  => 1,
                    'ffprobe.binaries' => '/usr/bin/ffprobe',
                ])
            );
         } catch (\Exception $e) {
             header("HTTP/1.1 500 Server Error : missing ffmpeg");
             http_response_code(500);
             die;
         }
   }

    public function modify(Container $di)
    {
    }
}
