<?php
namespace Aura\Web_Project\_Config;

use Aura\Di\Config;
use Aura\Di\Container;

class Common extends Config
{
    public function define(Container $di)
    {
        $di->params['Aura\View\TemplateRegistry']['paths'] = [
            dirname(__DIR__) . '/templates/views',
            dirname(__DIR__) . '/templates/layouts',
        ];
        $di->set('aura/project-kernel:logger', $di->lazyNew('Monolog\Logger'));
        $di->set('view', $di->lazyNew('Aura\View\View'));
        $di->set('media_files', $di->lazyNew(
            '\Models\MediaFiles', [
                'db' => $di->lazyNew('Aura\Sql\ExtendedPdo'),
                'query_factory' => $di->lazyNew(
                    'Aura\SqlQuery\QueryFactory',
                    ['mysql']
                ),
                'ffprobe' => $di->lazyGet('ffprobe'),
            ]
        ));
    }

    public function modify(Container $di)
    {
        $this->modifyLogger($di);
        $this->modifyWebRouter($di);
        $this->modifyWebDispatcher($di);
    }

    public function modifyLogger(Container $di)
    {
        $project = $di->get('project');
        $mode = $project->getMode();
        $file = $project->getPath("tmp/log/{$mode}.log");

        $logger = $di->get('aura/project-kernel:logger');
        $logger->pushHandler($di->newInstance(
            'Monolog\Handler\StreamHandler',
            array(
                'stream' => $file,
           )
        ));
    }

    public function modifyWebRouter(Container $di)
    {
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $path = strpos($path, 'jwebplay') ? 'jwebplay/' : NULL;

        $router = $di->get('aura/web-kernel:router');
        $router->add('scan', '/'.$path)
               ->setValues(['action' => 'scan']);

        $router->attachResource('mediafile', '/'.$path.'mediafile');
    }

    public function modifyWebDispatcher($di)
    {
        $folder = '/home/jerome/media';
        $view = $di->get('view');
        $dispatcher = $di->get('aura/web-kernel:dispatcher');
        $response = $di->get('aura/web-kernel:response');
        $request = $di->get('aura/web-kernel:request');
        $files = $di->get('media_files');

        $dispatcher->setObject(
            'mediafile.browse',
            function () use ($response, $request, $files) {
                $files = $files->all();
                $files = json_encode($files);
                $response->headers->set('Content-Type', 'application/json');
                $response->content->set($files);
            }
        );

        $dispatcher->setObject(
            'mediafile.read',
            function ($id) use ($response, $request, $files) {
                $file = $files->one($id);
                $file = json_encode($file);
                $response->headers->set('Content-Type', 'application/json');
                $response->content->set($file);
            }
        );

        $dispatcher->setObject(
            'mediafile.create',
            function () use ($response, $request, $files) {
                $post = $request->post->get();
                $files->insert($post);
                $file = json_encode($post);
                $response->headers->set('Content-Type', 'application/json');
                $response->content->set($file);
            }
        );

        $dispatcher->setObject(
            'scan',
            function () use ($view, $response, $request, $files, $folder) {
                //$view->setLayout('default');
                //$view->setView('hello');
                $view->setLayout('light');
                $view->setView('scan');
                $view->setData(
                    [
                        //'data' => $files->all(),
                        'dir' => $files->scan($folder),
                        'types' => $files->types(),
                    ]
                );

                $response->content->set($view->__invoke());
            }
        );
    }
}
