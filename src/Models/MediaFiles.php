<?php
namespace Models;

class MediaFiles {

    protected $db;
    protected $query_factory;
    protected $types = [];
    protected $fmime;
    protected $finfo;
    protected $ffprobe;

    public function __construct($db, $query_factory, $ffprobe)
    {
        $this->db = $db;
        $this->query_factory = $query_factory;
        $this->ffprobe = $ffprobe;
        $this->initInfo();
    }

    public function __destruct()
    {
        $this->closeInfo();
    }

    public function types()
    {
        return $this->types;
    }

    public function all()
    {
        $select = $this->query_factory->newSelect();
        $select->cols(['*'])->from('mediafile');
        $sth = $this->db->prepare($select->getStatement());
        $sth->execute($select->getBindValues());
        return $sth->fetchAll(\PDO::FETCH_OBJ);
    }

    public function one($id)
    {
        $select = $this->query_factory->newSelect();
        $select->cols(['*'])->from('mediafile')->where('id = '.$id);;
        $sth = $this->db->prepare($select->getStatement());
        $sth->execute($select->getBindValues());
        return $sth->fetch(\PDO::FETCH_OBJ);
    }

    public function scan($dir, $rec = false, $insert = false, $types = false, &$res = [], &$i = 0)
    {
        if (!$handle = opendir($dir)) {
            return false;
        }
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                $this->process($dir, $entry, $res, $i);
                if ($insert && $this->isMediaFile($res[$i]['info'])) {
                    $this->insert($res[$i]);
                }
                if ($types) {
                    $this->addType($res[$i]['info']);
                }
                if ($rec && $res[$i]['mime'] == 'directory') {
                    $subdir = $dir.'/'.$entry;
                    $this->scan($subdir, $rec, $insert, $types, $res, $i);
                }
            }
            $i++;
        }
        closedir($handle);

        return $res;
    }

    protected function process($dir, $entry, &$res, &$i)
    {
        $file = $dir.'/'.$entry;
        $res[$i]['name'] = $entry;
        $res[$i]['path'] = $dir;
        $res[$i]['created'] = date('Y-m-d H:i:s');
        $res[$i]['size'] = filesize($file);
        $res[$i]['md5'] = md5($file);
        $res[$i]['mime'] = finfo_file($this->fmime, $file);
        $res[$i]['info'] = finfo_file($this->finfo, $file);
        $res[$i]['ffprobe'] = ($this->isMediaFile($res[$i]['info']))
            ? $this->getMetadata($file)
            : null;
        $res[$i]['format_name'] = $res[$i]['ffprobe']['format_name'];
        $res[$i]['format_long_name'] =
            $res[$i]['ffprobe']['format_long_name'];
        $res[$i]['duration'] = $res[$i]['ffprobe']['duration'];
        $res[$i]['bit_rate'] = $res[$i]['ffprobe']['bit_rate'];
        //$res[$i]['tags'] = $res[$i]['ffprobe']['tags'];
    }

    public function insert($res)
    {
        $insert = $this->query_factory->newInsert();
        if (isset($res['ffprobe'])) { // XXX filter only cols
            unset($res['ffprobe']);
        }
        $insert->into('mediafile')->cols($res);
        $sth = $this->db->prepare($insert->getStatement());
        $sth->execute($insert->getBindValues());
    }

    protected function initInfo()
    {
        if (empty($this->fmime) || empty($this->finfo)) {
            $this->fmime = finfo_open(FILEINFO_MIME_TYPE);
            $this->finfo = finfo_open(FILEINFO_CONTINUE);
        }
    }
    
    protected function closeInfo()
    {
        if (!empty($this->fmime) || !empty($this->finfo)) {
            finfo_close($this->fmime);
            finfo_close($this->finfo);
        }
    }

    protected function addType($info)
    {
        if (!in_array($info, $this->types)) {
            $this->types[] = $info;
        }
    }

    protected function getMetadata($file)
    {
        $metadata = (array)$this->ffprobe->format($file);
        return array_shift($metadata);
    }

    protected function isMediaFile($info)
    {
        return stripos($info, 'mpeg') !== false
            || stripos($info, 'media') !== false
            || stripos($info, 'audio') !== false;
    }
}
