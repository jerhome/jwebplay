<?php // templates/layouts/light.php ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">
<head lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel='mask-icon' href='dutch.svg' color='orange'>--> <!-- PIN the image needs to be a square SVG image, with a transparent (or simply: no) background, and all vectors 100% black -->
    <!--<link rel="apple-touch-icon" href="touch-icon-iphone.png">--> <!-- iOS desktop icon -->
    <!--<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad.png">-->
    <!--<link rel="apple-touch-icon" sizes="180x180" href="touch-icon-iphone-retina.png">-->
    <!--<link rel="apple-touch-icon" sizes="167x167" href="touch-icon-ipad-retina.png">-->
    <!--<link rel="apple-touch-startup-image" href="/launch.png">-->
    <!--<meta name="apple-mobile-web-app-title" content="AppTitle">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <?php echo $this->title(); ?>
</head>
<body>
<?php echo $this->getContent(); ?>
</body>
</html>
