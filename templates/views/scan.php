<?php // templates/views/scan.php ?>
<?php $this->title()->set("jWebPlay"); ?>

<?php if (!empty($this->types)): ?>
<ol>
<?php foreach ($this->types as $type): ?>
    <li><?php echo $type ?></li>
<?php endforeach; ?>
</ol>
<?php endif; ?>

<div class="table table-responsive">
    <table class="table table-striped table-condensed">
        <thead>
        <tr>
            <th>file</th>
            <th>mime</th>
        </tr>
        </thead>
        <tbody>
<?php foreach ($this->dir as $entry): ?>
            <tr>
                <td><?php echo $entry['path'].'/'.$entry['name']; ?></td>
                <td><?php echo $entry['mime']; ?></td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
</div>